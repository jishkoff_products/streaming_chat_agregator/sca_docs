# SCA - Streaming Chat Agregator

В этом репозитории содержится публичная документация 
по проекту приложения чата агрегатора с нескольких 
стриминговых платформ. 

## Ссылки

[figma](https://www.figma.com/file/TnXHheq6wKUQPnI7tJHnLq/MegaChatAgregator?type=design&node-id=112-7&mode=design&t=N5BNGAI5lUCZZtQo-0)
